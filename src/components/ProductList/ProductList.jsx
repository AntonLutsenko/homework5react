import React from "react";
import ProductCard from "../ProductCard/ProductCard";
import { useSelector } from "react-redux";

import "./productList.scss";

const ProductList = ({ items }) => {
	const favorites = useSelector((state) => state.favorites.favorites);
	const cards = useSelector((state) => state.cards.cards);
	const favoriyeCards = items.map((elem) => {
		favorites.includes(elem.id)
			? (elem.isFavorite = true)
			: (elem.isFavorite = false);
		return elem;
	});
	return (
		<>
			<ul className="product-cards">
				{items.map((e) => (
					<ProductCard
						id={e.id}
						key={e.id}
						name={e.name}
						price={e.Price}
						routeImg={e.routeImg}
						color={e.color}
						className="favor-icon"
						isFavorite={e.isFavorite}
						inCard={cards.includes(e.id)}
					/>
				))}
			</ul>
		</>
	);
};

export default ProductList;
