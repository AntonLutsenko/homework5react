import React from "react";
import "./Button.scss";

const Button = ({ dataModal, className, onClick, text, id }) => {
	return (
		<button
			data-modal-id={dataModal}
			onClick={onClick}
			id={id}
			className={className}
		>
			{text}
		</button>
	);
};

export default Button;
