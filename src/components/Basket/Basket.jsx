import React from "react";
import { useSelector } from "react-redux";
import { Checkout } from "../Checkout/Checkout";
import ProductList from "../ProductList/ProductList";

const Basket = ({ filter }) => {
	const items = useSelector((state) => state.items.items);
	const cards = useSelector((state) => state.cards.cards);
	const basketCards = filter(items, cards);

	return (
		<>
			<ProductList items={basketCards} />
			{basketCards.length !== 0 && <Checkout />}
		</>
	);
};

export default Basket;
