import "./buy.scss";
const Buy = () => {
	return (
		<form className="checkout">
			<div className="checkout__block">
				<label htmlFor="">First name:</label>
				<input className="checkout__field" type="text"></input>
			</div>
			<div className="checkout__block">
				<label htmlFor="">Last name:</label>
				<input className="checkout__field" type="text"></input>
			</div>
			<div className="checkout__block">
				<label htmlFor="">Age:</label>
				<input className="checkout__field" type="number"></input>
			</div>
			<div className="checkout__block">
				<label htmlFor="">Address:</label>
				<input className="checkout__field" type="text"></input>
			</div>
			<div className="checkout__block">
				<label htmlFor="">Phone:</label>
				<input
					className="checkout__field"
					type=""
					// value="+38 (___) ___-__-__"
					inputmode="numeric"
				></input>
			</div>
			<div className="checkout__block">
				<button className="checkout__btn">checkout</button>
			</div>
		</form>
	);
};

export default Buy;
