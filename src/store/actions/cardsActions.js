export const recivedIdGoodsInCard = (cards) => {
	return {
		type: "RECEIVED_ID_GOODS_IN_CARD",
		payload: cards,
	};
};

export const checkout = (values) => {
	return {
		type: "CHECKOUT",
		payload: values,
	};
};
