import { combineReducers } from "redux";
import { cardReducer } from "./cardsReducer";
import { favoriteReducer } from "./favoriyeReducer";
import { itemsReducer } from "./itemsReducer";
import { modalReducer } from "./modalReducer";

const rootReducer = combineReducers({
	items: itemsReducer,

	favorites: favoriteReducer,
	modal: modalReducer,
	cards: cardReducer,
});

export default rootReducer;
